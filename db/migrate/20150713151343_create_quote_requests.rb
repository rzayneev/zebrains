class CreateQuoteRequests < ActiveRecord::Migration
  def change
    create_table :quote_requests do |t|
      t.string :name
      t.text :description
      t.string :email
      t.string :phone
      t.string :url
      t.string :company_name
      t.text :needs
      t.text :attachment

      t.timestamps null: false
    end
  end
end

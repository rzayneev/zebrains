class Notification < ApplicationMailer
  @@mail_to = 'info@zebrains.com'

  def send_new_quote_request(quote_request)
    @quote_request= quote_request
    mail(to: @@mail_to, subject: '[zebrains.com] New quote request')
  end
end

class ApplicationMailer < ActionMailer::Base
  default from: "info@zebrains.com"
  layout 'mailer'
end

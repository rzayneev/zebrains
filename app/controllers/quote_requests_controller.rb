class QuoteRequestsController < ApplicationController
  before_action :set_quote_request, only: [:show, :destroy]
  before_filter :authenticate, only: [:index, :destroy]

  # GET /quote_requests
  # GET /quote_requests.json
  def index
    @quote_requests = QuoteRequest.all
  end

  # DELETE /quote_requests/1
  # DELETE /quote_requests/1.json
  def destroy
    @quote_request.destroy
    respond_to do |format|
      format.html { redirect_to quote_requests_url, notice: 'Quote request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /quote_requests/new
  def new
    @quote_request = QuoteRequest.new
  end

  def success
  end

  # POST /quote_requests
  # POST /quote_requests.json
  def create
    @quote_request = QuoteRequest.new(quote_request_params)


    if @quote_request.save
      uploaded_io = params[:quote_request][:file]

      if (uploaded_io)
        @quote_request.attachment = uploaded_io.original_filename
        File.open(Rails.root.join('public', 'uploads', @quote_request.id.to_s+uploaded_io.original_filename), 'wb') do |file|
          file.write(uploaded_io.read)
        end
      end

      Notification.send_new_quote_request(@quote_request).deliver_now
      redirect_to success_quote_requests_path
    else
      render 'new'
    end
  end

  protected
    def authenticate
      authenticate_or_request_with_http_basic do |username, password|
        username == "ze" && password == "brains"
      end
    end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quote_request
      @quote_request = QuoteRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quote_request_params
      params.require(:quote_request).permit(:name, :email, :phone, :company_name, :url, :description, needs: [])
    end

end

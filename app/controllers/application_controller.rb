class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_locale

  layout "application"

  # def default_url_options(options={})
  #   { :locale => I18n.locale == I18n.default_locale ? nil : I18n.locale  }
  # end

  private
  def set_locale
    I18n.locale = extract_locale_from_domain || I18n.default_locale
    # I18n.locale = :en
  end

  def extract_locale_from_domain
    parsed_locale = request.host.split('.').last
    I18n.available_locales.map(&:to_s).include?(parsed_locale) ? parsed_locale : nil
  end
end

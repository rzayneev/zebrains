class PageController < ApplicationController

  def index
  end

  def about
  end

  def contact_us
  end

  def services
  end

  def portfolio
  end
end

// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require foundation/foundation
//= require foundation/foundation.abide
//= require foundation/foundation.accordion
//= require foundation/foundation.alert
//= require foundation/foundation.clearing
//= require foundation/foundation.dropdown
//= require foundation/foundation.equalizer
//= require foundation/foundation.interchange
//= require foundation/foundation.joyride
//= require foundation/foundation.magellan
//= require foundation/foundation.offcanvas
//= require foundation/foundation.orbit
//= require foundation/foundation.reveal
//= require foundation/foundation.slider
//= require foundation/foundation.tab
//= require foundation/foundation.tooltip
//= require foundation/foundation.topbar
//= require turbolinks
//= require slick.js/slick.js
//= require_tree .

var map;
var MY_MAPTYPE_ID = 'styled-map';
var offices = {};
var mapIds = [];

var mapStyle = [{
    "featureType": "all",
    "elementType": "all",
    "stylers": [{"saturation": -100}, {"lightness": "-62"}, {"color": "#5c5c5c"}]
}, {"featureType": "all", "elementType": "labels.text.fill", "stylers": [{"color": "#4f4f4f"}]}, {
    "featureType": "all",
    "elementType": "labels.text.stroke",
    "stylers": [{"color": "#353535"}, {"weight": "0.01"}]
}, {"featureType": "landscape", "elementType": "geometry", "stylers": [{"color": "#0d0d0d"}]}, {
    "featureType": "poi",
    "elementType": "geometry.fill",
    "stylers": [{"color": "#272727"}]
}, {
    "featureType": "poi",
    "elementType": "geometry.        stroke",
    "stylers": [{"color": "#808080"}]
}, {"featureType": "road", "elementType": "geometry", "stylers": [{"color": "#282828"}]}, {
    "featureType": "transit",
    "elementType": "labels",
    "stylers": [{"hue": "#ff0000"}, {"saturation": 100}, {"lightness": "-50"}, {"invert_lightness": true}, {"gamma": 1.5}]
}];

var initMap = function () {
    generateLocations();

    $(document).on('click', '.show-on-map', function () {
        var $this = $(this),
            $mapDiv = $('#' + $this.data('map')),
            mapId = $this.data('name');

        $(mapIds.toString()).removeClass('active');
        $mapDiv.addClass('active');
        $('.show-on-map').not($this).removeClass("active");
        $this.addClass('active');

        google.maps.event.trigger(offices[mapId].map, 'resize');
    });
};

function generateLocations() {
    $('.show-on-map').each(function () {
        var self = $(this),
            name = self.data('name'),
            mapId = self.data('map'),
            latlng = self.data('latlng'),
            mapCenter = self.data('center');

        offices[name] = new OfficeLocation(name, latlng, mapId, mapCenter);
        mapIds.push('#' + mapId);
    });
}

function OfficeLocation(name, latlng, mapId, mapCenter) {
    this.name = name;
    this.latlng = new google.maps.LatLng(latlng.latitude, latlng.longitude);
    this.mapCenter = new google.maps.LatLng(mapCenter.latitude, mapCenter.longitude);
    this.mapId = mapId;
    this.options = {
        zoom: 16,
        center: this.mapCenter,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, mapId]
        },
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
    };

    this.map = new google.maps.Map(document.getElementById(mapId), this.options);
    this.map.setOptions({styles: mapStyle});
    var ballonIcon = {
        path: 'M12.000,31.999 C12.000,31.999 0.000,19.911 0.000,11.999 C0.000,5.372 5.373,-0.001 12.000,-0.001 C18.627,-0.001 24.000,5.372 24.000,11.999 C24.000,19.769 12.000,31.999 12.000,31.999 ZM12.000,1.999 C6.477,1.999 2.000,6.533 2.000,12.124 C2.000,18.734 12.000,29.000 12.000,29.000 C12.000,29.000 22.000,18.667 22.000,12.124 C22.000,6.533 17.523,1.999 12.000,1.999 ZM12.000,16.000 C9.791,16.000 8.000,14.208 8.000,11.999 C8.000,9.790 9.791,7.999 12.000,7.999 C14.209,7.999 16.000,9.790 16.000,11.999 C16.000,14.208 14.209,16.000 12.000,16.000 ZM12.000,10.000 C10.895,10.000 10.000,10.895 10.000,11.999 C10.000,13.104 10.895,13.999 12.000,13.999 C13.105,13.999 14.000,13.104 14.000,11.999 C14.000,10.895 13.105,10.000 12.000,10.000 Z',
        fillColor: '#ec3a3a',
        fillOpacity: 1,
        scale: 1,
        strokeWeight: 0
    };

    this.marker = new google.maps.Marker({
        position: this.latlng,
        map: this.map,
        icon: ballonIcon
    });

    // this.map.setOptions({styles: mapStyle});
    // this.marker = new google.maps.Marker({
    //     position: this.LatLng,
    //     map: this.map,
    //     title: this.name
    // });
}

var initFoundation = function () {
    $(document).foundation({
        reveal: {
            animation: 'fade',
            close_on_background_click: true,
            close_on_esc: true
        },
        topbar: {
            scrolltop: false
        }
    });
};


var initFileUpload = function () {
    var wrapper = $(".file_upload"),
        inp = wrapper.find("input"),
        btn = wrapper.find(".icon-cloud-upload"),
        lbl = wrapper.find("mark");

    // Crutches for the :focus style:
    inp.focus(function () {
        wrapper.addClass("focus");
    }).blur(function () {
        wrapper.removeClass("focus");
    });

    var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;

    inp.change(function () {
        var file_name;
        if (file_api && inp[0].files[0])
            file_name = inp[0].files[0].name;
        else
            file_name = inp.val().replace("C:\\fakepath\\", '');

        if (!file_name.length)
            return;

        if (lbl.is(":visible")) {
            lbl.text(file_name);
        } else
            btn.text(file_name);
    }).change();

};

var initNavigation = function () {
    var stickyOffset = $('#navigation').offset().top;

    $(window).scroll(function () {
        var sticky = $('#navigation'),
            scroll = $(window).scrollTop();

        if (scroll >= stickyOffset) sticky.addClass('fixed');
        else sticky.removeClass('fixed');
    });
};


var initCarousel = function () {
    $(".slider").slick({
        centerMode: true,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 2000,
        infinite: true,
        slidesToShow: 3,
        centerPadding: '60px',
        arrows: true,
        prevArrow: '<span class="slick-prev"></span>',
        nextArrow: '<span class="slick-next"></span>',
        // nextArrow: '<i class="slick-next foundicon-right-arrow"></i>',
        // prevArrow: '<i class="slick-prev fa fa-arrow-left"></i>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 900,
                settings: {
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 700,
                settings: {
                    centerMode: false,
                    variableWidth: false,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });
};

$(document).on('ready page:load', function () {
    $(function () {
        initFileUpload();
        initMap();
        initNavigation();
        initFoundation();
        initCarousel();
    });
});


$(window).resize(function () {
    $(".file_upload input").triggerHandler("change");
});

$(document).ready(function () {
    var interval,
        $container = $(".js-container"),
        items = $('.js-contacts-item'),
        currentBotAvatar = '';

    function preparePost(msg) {
        var hours = new Date().getHours();
        var minutes = new Date().getMinutes();
        var html = '<div class="message ' + msg.position + '">';
        if (msg.position == 'left') {
            html += '<div class="contact-image"><img src="' + currentBotAvatar + '"></div>'
        }
        html += '<div class="message-holder">';
        if (msg.name) {
            html += '<p class="name">' + msg.name + '</p>';
        }
        html += '<div class="message-info">' + msg.text + '</div><div class="time">' + hours + ':' + (minutes < 10 ? '0' : '') + minutes + '</div></div></div>';

        return html;
    }

    function playChat(dialogs, pos) {
        $container.empty();
        var defer = $.Deferred();
        var messages = dialogs[pos] || dialogs[0];
        var t = 0;
        var $currentMenu = $(items.get(pos));
        currentBotAvatar = $currentMenu.find('.image-holder>img').attr('src');

        interval = setInterval(function () {
            sendMessage(messages[t]);
            if (t == messages.length - 1) {
                clearInterval(interval);
                pos++;
                setTimeout(function () {
                    defer.resolve(dialogs, pos);
                }, 4000);
            }
            t++;
        }, 3000);

        return defer.promise();
    }

    function playNextChat(dialogs, pos) {
        clearInterval(interval);
        pos = pos < dialogs.length ? pos : 0;
        items.removeClass('active');
        var $currentMenu = $(items.get(pos));
        $currentMenu.addClass('active');
        return playChat(dialogs, pos)
            .then(function (dialogs, nextPos) {
                playNextChat(dialogs, nextPos)
            });
    }

    function sendMessage(post) {
        var html = preparePost(post);
        $container.append(html);
        $container.animate({scrollTop: $container.offset().top + 150}, 'slow');
    }

    $.get("/dialogs_" + ($('html').attr('lang') || 'en') + ".json")
        .done(function (data) {
            items.on('click', function () {
                var pos = $(this).data('dialog');
                clearInterval(interval);
                playNextChat(data, pos);
            });

            //Autostart
            $('.js-contacts-item.active').click();
        })
        .fail(function (err) {
            console.log('error:', err);
        });

});

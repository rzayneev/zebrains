class QuoteRequest < ActiveRecord::Base
  serialize :needs, Array

  validate :contact_presence

  def contact_presence
    if phone.blank? and email.blank?
      errors.add(:base, 'need_contact_info')
      return false
    else
      return true
    end
  end
end

# Preview all emails at http://localhost:3000/rails/mailers/notification
class NotificationPreview < ActionMailer::Preview
  def send_new_quote_request
    Notification.send_new_quote_request(QuoteRequest.first)
  end
end
